var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var print = require('gulp-print');
var concat = require('gulp-concat');

var plugins = require('gulp-load-plugins')({
    rename: {
    'gulp-live-server': 'serve'
   }
});

var LessPluginCleanCSS = require('less-plugin-clean-css'),
    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
    cleancss = new LessPluginCleanCSS({ advanced: true }),
    autoprefix = new LessPluginAutoPrefix({ browsers: ["last 10 versions"] });
 
gulp.task('less', function () {
  return gulp.src(['./less/style.less'])
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ],
      plugins: [autoprefix, cleancss]
    }))
    .pipe(print())
    .pipe(gulp.dest('dist'))
    .pipe(plugins.livereload());
});

gulp.task('scripts', function() {
  gulp.src([
    './node_modules/jquery/dist/jquery.js',
    './node_modules/bootsrap-less/js/bootstrap.js',
      './node_modules/moment/min/moment-with-locales.min.js',
    './node_modules/moment-timezone/builds/moment-timezone-with-data.min.js',
    './js/scripts.js'
    ])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./dist/'))
});

gulp.task('default', ['less','scripts'], function() {
  plugins.livereload.listen();
  gulp.watch(['less/*.less','js/*.js'], ['less','scripts']);
});