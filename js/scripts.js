$(function() {
  //$.getJSON( "https://api.nasa.gov/planetary/apod?api_key=H1d7OYnOGWEaZE4FRAu7uLe4To0Lrdanyo1jtfWg", function( data ) {
  var img = Math.floor(Math.random() * 38) + 1; 
  console.log(img); 
   $('.bg-image').css('background-image','url(./img/bgs/' + img + '.jpg)').css('background-size','cover');
  
  //$('#clock').fitText(1.3);

  function update() {
    $('#clock-uk').html(moment().tz("Europe/London").format('HH:mm:ss'));
    $('#clock-usa-est').html(moment().tz("America/New_york").format('HH:mm:ss'));
    $('#clock-hungary').html(moment().tz("Europe/Budapest").format('HH:mm:ss'));
    $('#clock-slovenia').html(moment().tz("Europe/Belgrade").format('HH:mm:ss'));
  }
  setInterval(update, 1000);


});
