#Chrome Extension for NDP
Adding this extension will add a new tab page with useful links to tools we use.
Only people with the link below can access this extension
###Chrome store link
Visit [this link](https://goo.gl/bXiRJn "View extension in the chrome store") to add this to chrome.
###Developer notes
After cloning, run `npm insall`. 

Gulp compiles js and less to ./dist. Check gulpfile.js for tasks.

If adding to this tool please create a pull request.

###Updating
To recieve updates make sure chrome exstensions are in 'developer mode' by checking the box at the top of chromes extensions settings page.

###Chrome update
If you work on the project, submit changes via PR into master so rebuilding and zipping to chrome can be done.
The following zip should be send to chrome webstore
```zip -r ./ndp-dashboard.zip ndp-dashboard -x *.git*```

@kevinhowbrook
